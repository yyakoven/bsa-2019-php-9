<?php

namespace App\Http\Controllers;

use App\Entities\Photo;
use Illuminate\Support\Facades\Storage;
use App\Jobs\CropJob;

class PhotoController extends Controller
{
    public function store()
    {
        $user = auth()->user();
        $file = request()->file('photo');
        $name = $file->getClientOriginalName();
        $filepath = Storage::putFileAs('images/' . $user->id, $file, $name);
        $photo = Photo::create([
            'user_id' => $user->id,
            'original_photo' => $filepath,
            'status' => 'UPLOADED'
        ]);
        dispatch(new CropJob($photo));
    }
}
