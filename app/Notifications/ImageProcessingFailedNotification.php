<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Broadcasting\PrivateChannel;

class ImageProcessingFailedNotification extends Notification implements ShouldBroadcast
{
    use Queueable;

    public function __construct()
    {
        //
    }

    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Something went wrong and your images were not cropped')
                    ->action('Try Again', url('/'))
                    ->line('You should\'ve known better, now go do something useful');
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'status' => 'fail',
            'message' => 'there was a mistake'
        ]);
    }

    public function broadcastOn()
    {
        return new PrivateChannel('message');
    }
}
