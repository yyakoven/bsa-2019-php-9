<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Support\Facades\Storage;

class ImageProcessedNotification extends Notification implements ShouldBroadcast
{
    use Queueable;

    public $photo;

    public function __construct($photo)
    {
        $this->photo = $photo;
    }

    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Your images have been cropped')
                    ->line("Dear {$notifiable->name},")
                    ->line('Photos have been successfully uploaded and processed.')
                    ->line('Here are links to the images:')
                    ->action('Notification Action', url('/'))
                    ->line('Thanks!');
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'status' => 'success',
            'photo_100_100' => Storage::url($this->photo->photo_100_100),
            'photo_150_150' => Storage::url($this->photo->photo_150_150),
            'photo_250_250' => Storage::url($this->photo->photo_250_250)
            ]);
    }

    public function broadcastOn()
    {
        return new PrivateChannel('message');
    }
}
