<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $guarded= [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
