<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\PhotoService;
use App\Entities\Photo;
use Illuminate\Support\Facades\Storage;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use Exception;

class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $photo;
    protected $user;

    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
        $this->user = $this->photo->user;
    }

    public function handle(PhotoService $photoService)
    {
        $path = $this->photo->original_photo;
        $this->photo->update(['status' => 'PROCESSING']);
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        $name = pathinfo($path, PATHINFO_FILENAME);
        $sizes = ['100', '150', '250'];
        $status = 1;

        array_map(function($size)
            use($name, $path, $extension, $photoService, &$status) {
            try {
                $location = $photoService->crop($path, $size, $size);
                $newlocation = "images/{$this->photo->user_id}/{$name}{$size}x{$size}.{$extension}";
                if (Storage::exists($newlocation)) Storage::delete($newlocation);
                Storage::copy($location, $newlocation);
                $this->photo->update(["photo_{$size}_{$size}" => $newlocation]);
            } catch (Exception $e) {
                $status = 0;
            } finally {
                if (isset($location)) Storage::delete($location);
            }
        }, $sizes);
        $this->photo->update(['status' => $status ? 'SUCCESS' : 'FAIL']);

        if ($status) {
            $this->user->notify(new ImageProcessedNotification($this->photo));
        } else {
            $this->user->notify(new ImageProcessingFailedNotification());
        }
    }
}
